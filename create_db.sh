#!/usr/bin/env bash

if sudo -u postgres psql -lqt | cut --d \| --f 1 | grep -q 'dbcactus'; then
  echo 'Database already created'

else
  sudo -u postgres psql -c "CREATE DATABASE dbcactus;"
  sudo -u postgres psql -c "CREATE USER cactususer WITH ENCRYPTED PASSWORD 'test1234';"
  sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE dbcactus TO cactususer;"
  sudo -u postgres psql -c "ALTER USER cactususer CREATEDB;"

fi

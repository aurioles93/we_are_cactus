# We are Cactus #

Simple user management where the user can see, create and remove users 
BUT not edit user information unless that the user information update for his user.

### Building ###

* You can run `prepare_env.sh` to create/activate the virtual env
* You can run `create_db.sh` to create the database

### Deploy ###

You can run `run_app.sh` to execute the application.
Also this command create/activate the virtual env, create the db (if it is necessary) and run the migrations


### USAGE ###

You can login with an user credentials (Default credentials - email: `admin@test.com`, password: `admin1234`)
Now you can access to Django Admin via `[URL]/admin` or using the link in "home" page.
In django admin you can see the users were you can create and remove users; and edit your information.

Additionally, you can do the same queries via API, below you can find the API information.

### API ###

Here you can find the information related to API request. 
The authentication level is basic with the `email` and `password` of an user in the database.

You can use POSTMAN with the collection of the file `we_are_cactus.postman_collection.json`
or use the below request:

* List of users (GET)
  * URL: `/api/users/`
  * RESPONSE: 
  ```
  [{'id': ID, 'username': USERNAME, 'email': EMAIL, 'name': NAME_ALIAS, 'first_name': FIRST_NAME, 'last_name': LAST_NAME, 'avatar': IMAGE, 'last_login': LAST_LOGIN, 'password': ENCRYPTED_PASSWORD}]
  ```
* List of users using a query (GET)
  * URL: `/api/users/?q=[query]` where `query` if the information to look for. The search fields are `username`, `email`, `name`, `first_name`, `last_name` and `id`
  * RESPONSE: 
  ```
  [{'id': ID, 'username': USERNAME, 'email': EMAIL, 'name': NAME_ALIAS, 'first_name': FIRST_NAME, 'last_name': LAST_NAME, 'avatar': IMAGE, 'last_login': LAST_LOGIN, 'password': ENCRYPTED_PASSWORD}]
  ```
* Information of one user (GET)
  * URL: `/api/users/{id}/` where `id`is the id of the user.
  * RESPONSE: 
  ```
  {'id': ID, 'username': USERNAME, 'email': EMAIL, 'name': NAME_ALIAS, 'first_name': FIRST_NAME, 'last_name': LAST_NAME, 'avatar': IMAGE, 'last_login': LAST_LOGIN, 'password': ENCRYPTED_PASSWORD}
  ```
* Create an user (POST)
  * URL: `/api/users/` where the fields to send are:
    * `username` text field
    * `email` text field
    * `name` text field
    * `first_name` text field
    * `last_name` text field
    * `avatar` file (image) field
    * `password` text field
  * RESPONSE: 
  ```
  {'id': ID, 'username': USERNAME, 'email': EMAIL, 'name': NAME_ALIAS, 'first_name': FIRST_NAME, 'last_name': LAST_NAME, 'avatar': IMAGE, 'last_login': LAST_LOGIN, 'password': ENCRYPTED_PASSWORD}
  ```
* Edit the user information (PATCH)
  * URL: `/api/users/{id}/` where `id`is the id of the user and the information to edit is:
    * `username` text field
    * `email` text field
    * `name` text field
    * `first_name` text field
    * `last_name` text field
    * `avatar` file (image) field
    * `password` text field
  * RESPONSE: 
  ```
  {'id': ID, 'username': USERNAME, 'email': EMAIL, 'name': NAME_ALIAS, 'first_name': FIRST_NAME, 'last_name': LAST_NAME, 'avatar': IMAGE, 'last_login': LAST_LOGIN, 'password': ENCRYPTED_PASSWORD}
  ```
    * In case you try to edit the information of another user the response will be:
      * Message: `You cannot edit another user`
      * Status: `403`

The reponses will contain the serialized information of the user, that contains:
```python
{
'id': ID, 
'username': USERNAME, 
'email': EMAIL, 
'name': NAME_ALIAS, 
'first_name': FIRST_NAME, 
'last_name': LAST_NAME, 
'avatar': IMAGE, 
'last_login': LAST_LOGIN, 
'password': ENCRYPTED_PASSWORD
}
```

### Testing ###

* You can run `python manage.py test` to execute the tests

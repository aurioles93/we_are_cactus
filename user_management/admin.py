from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.core.exceptions import PermissionDenied

from user_management.models import User


class UserAdminForm(UserChangeForm):
    class Meta:
        model = User
        fields = "__all__"

    # def clean_same_user(self):
    #     if self.cleaned_data["name"] == self.request:
    #         raise forms.ValidationError("No Vampires")
    #
    #     return self.cleaned_data["first_name"]

    def clean_email(self):
        email = self.cleaned_data['email']
        if email and User.objects.filter(email=email).exclude(pk=self.instance.pk).exists():
            raise forms.ValidationError("This email already exists. You have to use another email address.")

        return email


@admin.register(User)
class UserAdmin(UserAdmin):
    search_fields = (
        'email__startwith',
        'first_name__icontains',
        'last_name__icontains',
        'username__icontains',
    )

    readonly_fields = (
        'is_superuser',
        'groups',
        'user_permissions',
        'is_active',
        'last_login',
        'is_staff',
    )

    fieldsets = (
        (None, {
            'fields': ('email', 'username', 'password')
        }),
        ('Personal Info', {
            'fields': ('name', 'first_name', 'last_name', 'avatar')
        }),
        ('Admin Info', {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions', 'last_login')
        }),
    )

    form = UserAdminForm

    def get_form(self, request, obj=None, **kwargs):
        defaults = {}
        if obj is None:
            defaults['form'] = self.add_form

        defaults.update(kwargs)
        form = super().get_form(request, obj, **defaults)
        if obj:
            form.base_fields["name"].label = "Name (Alias):"

        return form

    def change_view(self, request, object_id, form_url='', extra_context=None):
        if not request.user == User.objects.get(id=object_id):
            extra_context = extra_context or {}
            extra_context['show_save_and_add_another'] = False
            extra_context['show_save_and_continue'] = False
            extra_context['show_save'] = False

        return super().change_view(request, object_id, form_url='', extra_context=extra_context)
        # if not request.method == 'POST' or request.user == User.objects.get(username=request.POST.get('username')):
        #     return super().change_view(request, object_id, form_url='', extra_context=None)
        # else:
        #     raise PermissionDenied("This email already exists. You have to use another email address.")

    def has_add_permission(self, request):
        print(request.path)
        if 'change' in request.path and '/{}/'.format(request.user.id) not in request.path:
            return False
        else:
            return super().has_add_permission(request)

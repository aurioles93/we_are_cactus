from rest_framework import serializers

from user_management.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'name', 'first_name', 'last_name', 'avatar', 'last_login', 'password']
        # extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User(
            email=validated_data['email'],
            username=validated_data['username'],
            name=validated_data['name'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
            avatar=validated_data['avatar'],
        )
        user.set_password(validated_data['password'])
        user.save()
        return user

    def update(self, instance, validated_data):
        if validated_data.get('email'):
            instance.email = validated_data.get('email')

        if validated_data.get('username'):
            instance.username = validated_data.get('username')

        if validated_data.get('name'):
            instance.name = validated_data.get('name')

        if validated_data.get('first_name'):
            instance.first_name = validated_data.get('first_name')

        if validated_data.get('last_name'):
            instance.last_name = validated_data.get('last_name')

        if validated_data.get('avatar'):
            instance.avatar = validated_data.get('avatar')

        if validated_data.get('password'):
            instance.set_password(validated_data.get('password'))

        instance.save()
        return instance

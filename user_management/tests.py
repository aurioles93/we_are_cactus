import json
import os
import tempfile

from django.test import TestCase
from rest_framework import test
from rest_framework import status
from user_management.models import User
from we_are_cactus.settings import BASE_DIR


class UserTestCase(TestCase):
    def setUp(self):
        self.password = 'admin123'
        self.user = User(
            email='test_user@test.com',
            first_name='Testing',
            last_name='Testing',
            username='testing_login',
        )
        self.user.set_password(self.password)
        self.user.save()

    def test_login_user(self):
        client = test.APIClient()
        response = client.login(username=self.user.email, password=self.password)

        assert response

    def test_login_user_not_user(self):
        client = test.APIClient()
        response = client.login(username=self.user.email, password=self.password + '1')

        assert not response

    def test_get_users(self):
        client = test.APIClient()
        client.login(username=self.user.email, password=self.password)
        user = User.objects.get(email=self.user.email)
        expected_result = [
            {
                'id': user.id,
                'username': user.username,
                'email': user.email,
                'name': user.name,
                'first_name': user.first_name,
                'last_name': user.last_name,
                'avatar': None,
                'last_login': user.last_login.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
                'password': user.password,
            },
        ]

        response = client.get('/api/users/')
        result = json.loads(response.content)

        assert response.status_code == status.HTTP_200_OK
        assert result == expected_result

    def test_get_users_404(self):
        client = test.APIClient()
        client.login(username=self.user.email, password=self.password)

        response = client.get('/api/users/1/')

        assert response.status_code == status.HTTP_404_NOT_FOUND

    def test_get_one_user(self):
        user = User(
            email='alejandro@test.com',
            first_name='Alejandro',
            last_name='Espada',
            username='alejandro_username',
            name='alejandro_alias',
        )
        user.set_password(self.password)
        user.save()
        client = test.APIClient()
        client.login(username=self.user.email, password=self.password)
        expected_result = {
            'id': user.id,
            'username': user.username,
            'email': user.email,
            'name': user.name,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'avatar': None,
            'last_login': user.last_login,
            'password': user.password,
        }

        response = client.get('/api/users/{}/'.format(user.id))
        result = json.loads(response.content)

        assert response.status_code == status.HTTP_200_OK
        assert result == expected_result

    def test_post(self):
        from PIL import Image

        image = Image.new('RGB', (100, 100))
        tmp_file = tempfile.NamedTemporaryFile(suffix='.jpg')
        image.save(tmp_file)
        tmp_file.seek(0)

        expected_result = {
            'username': 'alejandro_post',
            'email': 'alejandro_post@test.com',
            'name': 'user_name',
            'first_name': 'Alejandro',
            'last_name': 'Espada',
            'avatar': tmp_file,
            'last_login': '',
            'password': self.password,
        }

        client = test.APIClient()
        client.login(username=self.user.email, password=self.password)
        response = client.post('/api/users/', expected_result, format='multipart')
        result = json.loads(response.content)
        # The last_login in database is None instead of ''
        expected_result['last_login'] = None
        # The path of the file is not known
        result.pop('avatar')
        expected_result.pop('avatar')
        # The id is not known until the user is created
        result.pop('id')
        # The hash of the password is different
        result.pop('password')
        expected_result.pop('password')

        assert response.status_code == status.HTTP_201_CREATED
        assert result == expected_result
        os.remove(os.path.join(BASE_DIR, User.objects.get(email=result['email']).avatar.path))

    def test_patch_same_user(self):
        client = test.APIClient()
        client.login(username=self.user.email, password=self.password)
        user = User.objects.get(email=self.user.email)

        expected_result = {
            'id': user.id,
            'username': user.username,
            'email': user.email,
            'name': user.name,
            'first_name': 'new_first_name',
            'last_name': 'new_last_name',
            'avatar': None,
            'last_login': user.last_login.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
            'password': user.password,
        }

        response = client.patch(
            '/api/users/{}/'.format(user.id),
            {
                'first_name': 'new_first_name',
                'last_name': 'new_last_name',
            },
            format='multipart',
        )
        result = json.loads(response.content)

        assert response.status_code == status.HTTP_200_OK
        assert result == expected_result

    def test_patch_different_user(self):
        user = User(
            email='alejandro@test.com',
            first_name='Alejandro',
            last_name='Espada',
            username='alejandro_username',
            name='alejandro_alias',
        )
        user.set_password(self.password)
        user.save()
        client = test.APIClient()
        client.login(username=self.user.email, password=self.password)

        expected_result = 'You cannot edit another user'

        response = client.patch(
            '/api/users/{}/'.format(user.id),
            {
                'first_name': 'new_first_name',
                'last_name': 'new_last_name',
            },
            format='multipart',
        )
        result = json.loads(response.content)

        assert response.status_code == status.HTTP_403_FORBIDDEN
        assert result == expected_result

import sys

from django.db import migrations
from django.contrib.auth.hashers import make_password


def create_periodic_tasks(apps, schema_editor):
    user_model = apps.get_model("user_management", "User")
    db_alias = schema_editor.connection.alias
    admin_user, _ = user_model.objects.using(db_alias).get_or_create(
        email='admin@test.com',
        first_name='User',
        last_name='Admin',
        name='admin',
        username='admin',
        is_active=True,
        is_superuser=True,
        is_staff=True,
        password=make_password('admin1234'),
    )
    test_user, _ = user_model.objects.using(db_alias).get_or_create(
        email='test@test.com',
        first_name='User',
        last_name='Test',
        name='test',
        username='test',
        is_active=True,
        is_superuser=True,
        is_staff=True,
        password=make_password('test1234'),
    )
    user, _ = user_model.objects.using(db_alias).get_or_create(
        email='user@test.com',
        first_name='User',
        last_name='user',
        name='user',
        username='user',
        is_active=True,
        is_superuser=True,
        is_staff=True,
        password=make_password('user1234'),
    )


def delete_periodic_tasks(apps, schema_editor):
    user_model = apps.get_model("user_management", "User")
    db_alias = schema_editor.connection.alias
    user_model.objects.using(db_alias).filter(email='admin@test.com').delete()
    user_model.objects.using(db_alias).filter(email='test@test.com').delete()
    user_model.objects.using(db_alias).filter(email='user@test.com').delete()


class Migration(migrations.Migration):

    dependencies = [('user_management', '0001_initial')]

    operations = [
        migrations.RunPython(create_periodic_tasks, delete_periodic_tasks),
    ] if 'test' not in sys.argv[1:] else []

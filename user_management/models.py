import re
import we_are_cactus.settings as settings

from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.core import validators
from django.db import models
from django.utils.translation import ugettext_lazy as _


class UserManager(BaseUserManager):
    def create_user(self, name='', email='', password=None):
        """Creates and saves a User."""

        user = self.model(
            name=name,
            email=email
        )
        user.set_password(password)

        user.save(using=self._db)

        return user

    def create_superuser(self, name='', email='', password=None):
        """Creates and saves a superuser with the given email, date of birth and password."""

        user = self.model(
            name=name,
            email=email,
            is_active=True,
            is_superuser=True,
        )
        user.set_password(password)

        user.save(using=self._db)

        return user


class User(AbstractBaseUser, PermissionsMixin):

    email = models.CharField(_('email address'), max_length=30, unique=True,
                             validators=[
                                 validators.RegexValidator(re.compile(r'^[^@]+@[^@]+\.[^@]+$'),
                                                           _('Enter a valid email address.'), 'invalid')
                             ])
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    name = models.CharField(_('name'), max_length=30, blank=True)
    is_active = models.BooleanField(_('active'), default=True,
                                    help_text=_('Designates whether this user should be treated as '
                                                'active. Unselect this instead of deleting accounts.'))
    is_staff = models.BooleanField(_('staff status'), default=False,
                                   help_text=_('Designates that this user has all permissions without '
                                               'explicitly assigning them.'))
    username = models.CharField(_('username'), unique=True, max_length=30)
    avatar = models.ImageField(blank=True, upload_to='static')

    objects = UserManager()

    USERNAME_FIELD = 'email'

    def get_absolute_url(self):
        return "/users/%s/" % self.username

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        "Returns the short name for the user."
        return self.first_name

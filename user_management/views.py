from django.contrib.auth.context_processors import auth
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from rest_framework import filters, permissions, viewsets, status
from rest_framework.response import Response

from user_management.models import User
from user_management.serializers import UserSerializer


@csrf_protect
@login_required
def home(request):
    template = 'home.html'
    user = request.user

    context = {'user': user}

    return render(request, template, context)


@csrf_protect
def login(request):
    template = 'user_management/login.html'
    context = {}
    if request.method == 'POST':
        form = request.POST
        username = form.get('username', '')
        password = form.get('password', '')
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                auth.login(request, user)
                # Redirect to a success page.
                # return redirect(home)
                template = 'home.html'

                return render(request, template, context={'user': user})
            else:
                # Return a 'disabled account' error message
                context = {'message': 1}
        else:
            context = {'message': 1}
    return render(request, template, context)


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('id')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]
    filter_backends = [filters.SearchFilter]
    search_fields = ['username', 'email', 'name', 'first_name', 'last_name', 'id']

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        if not instance == request.user:
            return Response('You cannot edit another user', status=status.HTTP_403_FORBIDDEN)
        else:
            return super().update(request, *args, **kwargs)


def custom_404(request):
    return render(request, '404.html', {}, status=404)

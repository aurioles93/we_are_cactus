#!/usr/bin/env bash

source prepare_env.sh
source create_db.sh
python manage.py migrate
python manage.py runserver

#!/usr/bin/env bash

VIRTUAL_ENV="../cactus_env/bin/"

if [ -d "$VIRTUAL_ENV" ]; then
  source ${VIRTUAL_ENV}/activate

else
  cd ..
  python3 -m venv cactus_env
  cd we_are_cactus
  source ${VIRTUAL_ENV}/activate

fi

pip install -r requirements.txt

export DB_NAME=dbcactus
export DB_USER=cactususer
export DB_PASSWORD=test1234
